A markdown plugin for notepad++.
====

This is fork of NppMarkdown. 
https://github.com/plxaye/NppMarkdown

version 1.0

Thanks Pan Jiajie for NppMarkdown.
Thanks for Chad Nelson'Cpp-Markdown lib.

Bulding NppMarkdown2 at Windows
-------------------------------

To build NppMarkdown2 on your computer you will need these packages:

1. meson
2. ninja
3. mingw-w64 (i686) or Microsoft Visual C++ compiler

Notice: If you have many compilers then use environment variable CXX for specify
    set CXX=cl.exe
    
Git clone the sources:
    git clone https://bitbucket.org/RedSkotina/nppmarkdown2
Run meson and build NppMarkdown2:
    cd nppmarkdown2
    mkdir build
    cd build
    meson ..
    ninja

Installation
------------
1. Copy NppMarkdown2.dll to folder `<Notepad++ Install Dir>\plugins`
2. Restart NPP. It will now appear as a menu item under Plugins->NppMarkdown2

Usage
-----

1. Enable NppMarkdown2 panel with `Plugins->NppMarkdown2->Enable`
2. Press button `Preview` at NppMarkdown2 panel