// dllmain.cpp : Defines the entry point for the DLL application.
#include "PluginDefinition.h"
// #include "PreViewDlg.h"
#include "NppMarkdown2Dlg.h"

/*
#include <atlfile.h>
#include <vector>
#include <fstream>
#include <sstream>
#include <process.h>
#include "markdown.h"
#include <atlconv.h>
#include <atlctrls.h> */
//using namespace std;

extern FuncItem funcItem[nbFunc];
extern NppData nppData;
//CAppModule _Module;

HMODULE g_Module;

extern NppMarkdown2Dlg _goToLine;
extern BOOL bReady;


BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  reasonForCall,
                       LPVOID lpReserved
					 )
{
	switch (reasonForCall)
	{
        case DLL_PROCESS_ATTACH:
            pluginInit(hModule);
            g_Module = hModule;
            break;
        case DLL_PROCESS_DETACH:
            commandMenuCleanUp();
            pluginCleanUp();
            break;
        case DLL_THREAD_ATTACH:
            break;
        case DLL_THREAD_DETACH:
            break;
        
	}
	return TRUE;
}

extern "C" __declspec(dllexport) void setInfo(NppData notpadPlusData)
{
	nppData = notpadPlusData;
	commandMenuInit();
}

extern "C" __declspec(dllexport) const TCHAR * getName()
{
	return NPP_PLUGIN_NAME;
}

extern "C" __declspec(dllexport) FuncItem * getFuncsArray(int *nbF)
{
	*nbF = nbFunc;
	return funcItem;
}

extern "C" __declspec(dllexport) void beNotified(SCNotification *notifyCode)
{
    switch (notifyCode->nmhdr.code) 
	{
        case NPPN_SHUTDOWN:
		{
			commandMenuCleanUp();
		}
		break;
        
        case SCN_MODIFIED:
           /* if (bReady)
            {
                if (_goToLine.dlg)
                {
                    CButton check;
                    HWND hwndCheck = ::GetDlgItem(_goToLine.dlg->m_hWnd, IDC_CHECK_LIVE_PREVIEW);
                    check.Attach(hwndCheck);
                    BOOL bCheck = check.GetCheck();
                    if (!bCheck)
                    {
                        return;
                    }
                    _goToLine.dlg->Tans();
                }
            }*/
   
        default:
			return;
    }
}


// Here you can process the Npp Messages 
// I will make the messages accessible little by little, according to the need of plugin development.
// Please let me know if you need to access to some messages :
// http://sourceforge.net/forum/forum.php?forum_id=482781
//
extern "C" __declspec(dllexport) LRESULT messageProc(UINT Message, WPARAM wParam, LPARAM lParam)
{
	return TRUE;
}

#ifdef UNICODE
extern "C" __declspec(dllexport) BOOL isUnicode()
{
	return TRUE;
}
#endif //UNICODE
