import sys
from os.path import basename, splitext
import argparse
import subprocess

def run_command(command):
    p = subprocess.Popen(command, shell=True,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT)
    return p.communicate()

parser = argparse.ArgumentParser()
parser.add_argument('rcfile')
parser.add_argument('objfile')
args = parser.parse_args()

if args.rcfile is None or args.objfile is None:
    sys.exit("Not enough arguments")

res_file_head, res_file_tail = splitext(basename(args.rcfile))    

run_command("rc.exe /fo {0}.res {1}".format(res_file_head, args.rcfile))
run_command("cvtres.exe /out:{0} {1}.res".format(args.objfile, res_file_head))


