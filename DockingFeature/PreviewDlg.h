#ifndef PREVIEW_DLG_H
#define PREVIEW_DLG_H

#include "BaseDialog.h"



class PreviewDlg : public CBaseDialog  
{
	public:

		PreviewDlg(int nResId, HWND hParent=NULL, HINSTANCE hInst=NULL);
		virtual ~PreviewDlg();

        void On_WM_INITDIALOG(HWND hDlg, WPARAM wParam, LPARAM lParam);
		void On_WM_COMMAND(HWND hDlg, WPARAM wParam, LPARAM lParam);

        virtual void Align(INT nWidth, INT nHeight);
        void Preview();
		DECLARE_MESSAGE_HANDLER(PreviewDlg);

};

#endif //PREVIEW_DLG_H