// BaseDialog.cpp: implementation of the CBaseDialog class.
//
//////////////////////////////////////////////////////////////////////

#include "BaseDialog.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CBaseDialog* CBaseDialog::_this_INIT_DIALOG = NULL;
HINSTANCE CBaseDialog::_hInst = NULL;
std::map<HWND, CBaseDialog* >	CBaseDialog::_DialogInstancesMap;

CBaseDialog::CBaseDialog(int nResId, HWND hParent, HINSTANCE hInst)
{
	_this_INIT_DIALOG = this; /// store this pointer

	m_nResId = nResId;
	m_hParent = hParent;
    _hInst = hInst; 
    m_hWindow = NULL;

}

CBaseDialog::~CBaseDialog()
{
    std::map<HWND, CBaseDialog*>::iterator	_DialogInstancesIterator = _DialogInstancesMap.find(m_hWindow); /// find message entry by key
    if(_DialogInstancesIterator != _DialogInstancesMap.end())
        _DialogInstancesMap.erase(_DialogInstancesIterator);
	m_hWindow = NULL;
	_this_INIT_DIALOG = NULL;
}

HWND CBaseDialog::create(void)
{
	//return(DialogBox(GetModuleHandle(NULL), MAKEINTRESOURCE(m_nResId), m_hParent, DialogProcStatic));
	m_hWindow = CreateDialogParam((_hInst)?_hInst:GetModuleHandle(NULL), MAKEINTRESOURCE(m_nResId), m_hParent, DialogProcStatic, 0);
   	_DialogInstancesMap.insert(std::map<HWND, CBaseDialog*>::value_type(m_hWindow, this)); /// insert key & data to map

    return m_hWindow;
}

void CBaseDialog::AddMessageHandler(UINT MessageId, void(CBaseDialog::*MsgHandler)(HWND hDlg,WPARAM wParam,LPARAM lParam))
{
	t_MessageEntry MessageEntry;
	MessageEntry.MsgHandler = MsgHandler;

	m_MessageMap.insert(std::map<UINT,t_MessageEntry>::value_type(MessageId, MessageEntry)); /// insert key & data to map
}

BOOL CALLBACK CBaseDialog::DialogProcStatic(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    std::map<HWND, CBaseDialog*>::iterator	_DialogInstancesIterator = _DialogInstancesMap.find(hDlg); /// find message entry by key
    
    // First message: WM_INITDIALOG, direct passthrough to dialogproc
	if(_DialogInstancesIterator == _DialogInstancesMap.end() && message == WM_INITDIALOG)
	{
		_this_INIT_DIALOG->DialogProc(hDlg, message, wParam, lParam);
	}
    
    if(_DialogInstancesIterator == _DialogInstancesMap.end()) /// check if message entry available
	{
		return FALSE;
	}
	else
	{
        CBaseDialog* pThis = (*_DialogInstancesIterator).second; /// dereference iterator and get message entry
		return(pThis->DialogProc(hDlg, message, wParam, lParam));
	}
    
	//CBaseDialog *pThis = (CBaseDialog*)m_lSaveThis; /// typecast stored this-pointer to CBaseDialog pointer

	
}

BOOL CALLBACK CBaseDialog::DialogProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	m_MessageMapIterator = m_MessageMap.find(message); /// find message entry by key
    //std::wstringstream  ss;
    //ss<<message;
    //MessageBox(NULL, ss.str().c_str(), NULL, MB_OK);
	if(m_MessageMapIterator == m_MessageMap.end()) /// check if message entry available
	{
		return FALSE;
	}
	else
	{
		t_MessageEntry MessageEntry = (*m_MessageMapIterator).second; /// dereference iterator and get message entry

		void (CBaseDialog::*MessageHandler)(HWND hDlg,WPARAM wParam,LPARAM lParam);
				
		MessageHandler = MessageEntry.MsgHandler;

		(this->*MessageHandler)(hDlg, wParam, lParam); /// execute function

		return TRUE;
	}
}
/*
void CBaseDialog::OnOK(void)
{
	EndDialog(m_hWindow, IDOK);
}

void CBaseDialog::OnCancel(void)
{
	EndDialog(m_hWindow, IDCANCEL);
}
*/
void CBaseDialog::MoveWindow(LPRECT rc, BOOL bRepaint)
{
	::MoveWindow(m_hWindow, rc->left, rc->top, rc->right, rc->bottom, bRepaint);
}


