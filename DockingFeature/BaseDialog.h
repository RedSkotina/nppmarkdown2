// BaseDialog.h: interface for the CBaseDialog class.
//
//////////////////////////////////////////////////////////////////////

#ifndef BASE_DIALOG_H
#define BASE_DIALOG_H

#include <windows.h>
#include <stdio.h>
#include <map>
#include <sstream>
#include "resource.h"
#include "messages.h"
class CBaseDialog;

typedef void(CBaseDialog::*fpMessageHandler)(HWND hDlg,WPARAM wParam,LPARAM lParam);

struct t_MessageEntry
{
	fpMessageHandler MsgHandler;
};

/////////////////////////////// MACROS

#define IMPLEMENT_MESSAGE_HANDLER(base,derived) void derived::AddHandler(UINT MessageId, void(derived::*Handler)(HWND hDlg,WPARAM wParam,LPARAM lParam))\
												{\
													AddMessageHandler(MessageId, (void(base::*)(HWND hDlg,WPARAM wParam,LPARAM lParam))Handler);\
												}\

#define DECLARE_MESSAGE_HANDLER(derived) void AddHandler(UINT MessageId, void(derived::*Handler)(HWND hDlg,WPARAM wParam,LPARAM lParam));\
										 void HandleManager(void);\

#define BEGIN_MESSAGE_MAP(derived) void derived::HandleManager(void) {

#define ADD_MESSAGE_HANDLER(message,handler) AddHandler(message, handler);

#define END_MESSAGE_MAP() }

#define ENABLE_MESSAGE_MAP() HandleManager();

class CBaseDialog  
{

	public:

		std::map<UINT,t_MessageEntry>				m_MessageMap;
		std::map<UINT,t_MessageEntry>::iterator	m_MessageMapIterator;

		CBaseDialog(int nResId, HWND hParent=NULL, HINSTANCE hInst=NULL);
		virtual ~CBaseDialog();

		HWND create(void);
		
		static BOOL CALLBACK DialogProcStatic(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
		BOOL CALLBACK DialogProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);

		void AddMessageHandler(UINT MessageId, void(CBaseDialog::*Handler)(HWND hDlg,WPARAM wParam,LPARAM lParam));

    public:
        void MoveWindow(LPRECT rc, BOOL bRepaint);
	
        virtual void Align(INT nWidth, INT nHeight) {};
	
    protected:
		
		int m_nResId;
		HWND m_hParent;
        
        HWND m_hWindow;
		//static long m_lSaveThis;
        static HINSTANCE _hInst;
        static std::map<HWND, CBaseDialog* >	_DialogInstancesMap;
		static CBaseDialog* _this_INIT_DIALOG; // dirty hack, because dialogproc WM_INITDIALOG executed before save THIS in map
        
};


#endif //BASE_DIALOG_H
