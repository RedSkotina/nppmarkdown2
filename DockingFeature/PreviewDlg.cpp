#include "PreviewDlg.h"
#define CPPWEBPAGE_STATIC
#include "cppwebpage.h"
#include "markdown.h"
#include "PluginInterface.h"
#include <locale>
#include <codecvt>
extern NppData nppData;

IMPLEMENT_MESSAGE_HANDLER(CBaseDialog, PreviewDlg)

BEGIN_MESSAGE_MAP(PreviewDlg)
	ADD_MESSAGE_HANDLER(WM_COMMAND, &PreviewDlg::On_WM_COMMAND)
    ADD_MESSAGE_HANDLER(WM_INITDIALOG, &PreviewDlg::On_WM_INITDIALOG)
END_MESSAGE_MAP()

PreviewDlg::PreviewDlg(int nResId, HWND hParent, HINSTANCE hInst) : CBaseDialog(nResId, hParent, hInst)
{
	ENABLE_MESSAGE_MAP();
}

PreviewDlg::~PreviewDlg()
{
    UnEmbedBrowserObject(m_hWindow);
}

void PreviewDlg::On_WM_INITDIALOG(HWND hDlg, WPARAM wParam, LPARAM lParam)
{
    // initialise web
    if (EmbedBrowserObject(hDlg)) 
        MessageBox(NULL, TEXT("MessageBox Text"), TEXT("ERRROR EMBED"), MB_OK);
    //LPCTSTR lpszTest2 = TEXT("<P>This is a picture.<P><IMG src=\"mypic.jpg\"><P>This is a picture.<P><IMG src=\"mypic.jpg\"><P>This is a picture.<P><IMG src=\"mypic.jpg\"><P>This is a picture.<P><IMG src=\"mypic.jpg\"><P>This is a picture.<P><IMG src=\"mypic.jpg\"><P>This is a picture.<P><IMG src=\"mypic.jpg\"><P>This is a picture.<P><IMG src=\"mypic.jpg\"><P>This is a picture.<P><IMG src=\"mypic.jpg\"><P>3This is a picture.<P><IMG src=\"mypic.jpg\"<P>This is a picture.<P><IMG src=\"mypic.jpg\"><P>This is a picture.<P><IMG src=\"mypic.jpg\"><P>This is a picture.<P><IMG src=\"mypic.jpg\"><P>This is a picture.<P><IMG src=\"mypic.jpg\"><P>This is a picture.<P><IMG src=\"mypic.jpg\">>");
    //DisplayHTMLStr(hDlg, lpszTest2);

    
}
void PreviewDlg::On_WM_COMMAND(HWND hDlg, WPARAM wParam, LPARAM lParam)
{
	switch(LOWORD(wParam))
    {
        case IDOK:
		{
			//OnOK();
		}
		break;

		case IDCANCEL:
		{
			//OnCancel();
		}
    }
}

void PreviewDlg::Align(INT nWidth, INT nHeight)
{
    //HWND hLiveChkBox = GetDlgItem(hDlg, ID_CHK_LIVE);
    //HWND hPreviewButton = GetDlgItem(hDlg, ID_BTN_PREVIEW);
    
    
    RECT dlu_offset = {0, 0, 0, -36};
    ::MapDialogRect(m_hParent, &dlu_offset); //convert DLU to pixels
    // x, y, width, height
    RECT rc = {0 , 0, nWidth, nHeight};
    rc.left += dlu_offset.left;
    rc.top += dlu_offset.top;
    rc.right += dlu_offset.right;
    rc.bottom += dlu_offset.bottom;
    rc.left = (rc.left < 0) ? 0 : rc.left;
    rc.top = (rc.top < 0) ? 0 : rc.top;
    rc.right = (rc.right < 0) ? 0 : rc.right;
    rc.bottom = (rc.bottom < 0) ? 0 : rc.bottom;
    
    
    // Move this dialog
    //::GetClientRect(m_hParent, &rc);
    
    this->MoveWindow(&rc, TRUE);
    
    ResizeBrowser(m_hWindow, rc.right, rc.bottom);

}

void PreviewDlg::Preview()
{
    int cur_scn = -1;
	::SendMessage(nppData._nppHandle, NPPM_GETCURRENTSCINTILLA, 0, (LPARAM)&cur_scn);
	if (cur_scn == -1)
        return;

    HWND curScintilla = (cur_scn == 0)?nppData._scintillaMainHandle:nppData._scintillaSecondHandle;
    int len = ::SendMessage(curScintilla, SCI_GETTEXTLENGTH, 0, 0);
    len += 1;
    char* buf = new char[len];
    //memset(buf, '\0', sizeof(char)*len);
    ::SendMessage(curScintilla, SCI_GETTEXT, len, (LPARAM)buf);
     
    std::string md_text = buf;
    markdown::Document md_doc;
    md_doc.read(md_text);
    //MessageBox(NULL, TEXT("MessageBox Text"), TEXT("ERRROR EMBED"), MB_OK);
    std::ostringstream stream;
    md_doc.write(stream);
    std::string html_text =  stream.str();
    //MessageBox(NULL, TEXT("MessageBox Text"), TEXT("ERRROR EMBED"), MB_OK);
    //USES_CONVERSION;
    int codepage = (int)::SendMessage(curScintilla, SCI_GETCODEPAGE, 0, 0);
    std::wstring wHtml;
    std::wstring_convert<std::codecvt_utf8<wchar_t>> myconv;
    switch (codepage)
    {
    case (int)SC_CHARSET_ANSI:
    case (int)SC_CHARSET_GB2312:
    case (int)936: // GBK
      //wHtml = A2W(html_text.c_str());
      break;
    case (int)SC_CP_UTF8:
      wHtml = myconv.from_bytes(html_text);
      break;
      // ADD YOUR ENCODING HERE ...
    default:
      wHtml = _T("Not supported encoding (UTF8/GB2312/GBK/ANSI)");
      break;
    }

    DisplayHTMLStr(m_hWindow, wHtml.c_str());
	delete[] buf;
    
}
