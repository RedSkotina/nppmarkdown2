#include "ControlDlg.h"

IMPLEMENT_MESSAGE_HANDLER(CBaseDialog, ControlDlg)

BEGIN_MESSAGE_MAP(ControlDlg)
	ADD_MESSAGE_HANDLER(WM_COMMAND, &ControlDlg::On_WM_COMMAND)
    ADD_MESSAGE_HANDLER(WM_INITDIALOG, &ControlDlg::On_WM_INITDIALOG)
END_MESSAGE_MAP()

ControlDlg::ControlDlg(int nResId, HWND hParent, HINSTANCE hInst) : CBaseDialog(nResId, hParent, hInst)
{
	ENABLE_MESSAGE_MAP();
}

ControlDlg::~ControlDlg()
{

}

void ControlDlg::On_WM_INITDIALOG(HWND hDlg, WPARAM wParam, LPARAM lParam)
{

}

void ControlDlg::On_WM_COMMAND(HWND hDlg, WPARAM wParam, LPARAM lParam)
{
    // catch buttons
	switch(LOWORD(wParam))
    {
        case ID_BTN_PREVIEW:
		{
			OnBtnPreview();
		}
		break;

    }
}

void ControlDlg::Align(INT nWidth, INT nHeight)
{
    //HWND hLiveChkBox = GetDlgItem(hDlg, ID_CHK_LIVE);
    //HWND hPreviewButton = GetDlgItem(hDlg, ID_BTN_PREVIEW);
    //MessageBox(NULL,TEXT("WM_SIZE CONTR"), NULL, MB_OK);
    RECT dlu_offset = {-189, -34, 189, 34};
    ::MapDialogRect(m_hParent, &dlu_offset); //convert DLU to pixels
      
    //std::wstringstream  ss;
	//ss<<TEXT("[")<<nWidth<<TEXT(",")<<nHeight<<TEXT("] ");
    //MessageBox(NULL, ss.str().c_str(), NULL, MB_OK);
    
    RECT rc = {nWidth, nHeight, 0, 0};
    rc.left += dlu_offset.left;
    rc.top += dlu_offset.top;
    rc.right += dlu_offset.right;
    rc.bottom += dlu_offset.bottom;
    rc.left = (rc.left < 0) ? 0 : rc.left;
    rc.top = (rc.top < 0) ? 0 : rc.top;
    rc.right = (rc.right < 0) ? 0 : rc.right;
    rc.bottom = (rc.bottom < 0) ? 0 : rc.bottom;
 
    this->MoveWindow(&rc, TRUE);
}

void ControlDlg::OnBtnPreview()
{
    ::PostMessage(m_hParent, NPPMARKDOWN2_PREVIEW , 0, 0);
	
    //MessageBox(NULL,TEXT("WM_SIZE CONTR"), NULL, MB_OK);
}