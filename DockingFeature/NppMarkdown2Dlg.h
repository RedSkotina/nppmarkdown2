//this file is part of notepad++
//Copyright (C)2003 Don HO ( donho@altern.org )
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either
//version 2 of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#ifndef NPPMARKDOWN2_DLG_H
#define NPPMARKDOWN2_DLG_H

#include "DockingDlgInterface.h"
#include "resource.h"

#include "PreviewDlg.h"
#include "ControlDlg.h"



class NppMarkdown2Dlg : public DockingDlgInterface
{
public :
	NppMarkdown2Dlg() : DockingDlgInterface(IDD_MAIN),
                        control_dlg(NULL),
                        preview_dlg(NULL)
    {
    };

    virtual void display(bool toShow = true) const 
    {
        DockingDlgInterface::display(toShow);
    };

	void setParent(HWND parent2set)
    {
		_hParent = parent2set;
	};

    PreviewDlg* preview_dlg;
    ControlDlg* control_dlg;
    BOOL is_ole_owner = FALSE;

	void create(tTbData * data, bool isRTL = false)
	{
		DockingDlgInterface::create(data, isRTL);
        
        
        //Try initialize OLE
        HRESULT ole_hres = OleInitialize(NULL);
        if (ole_hres != S_OK && ole_hres != S_FALSE)
        {
            std::wstringstream os;
            os << (LONG)ole_hres;
            MessageBox(NULL, os.str().c_str(), TEXT("OleInitialize Result"), MB_SETFOREGROUND);
            return;
        }
        if (ole_hres == S_OK)
            is_ole_owner = TRUE;
        
        // Create child dialogs
        preview_dlg = new PreviewDlg(IDD_PREVIEW, _hSelf, _hInst);
        HWND preview_hwnd = preview_dlg->create();
        ::ShowWindow(preview_hwnd, SW_SHOW);
        
        control_dlg = new ControlDlg(IDD_CONTROL, _hSelf, _hInst);
        HWND control_hwnd = control_dlg->create();
        ::ShowWindow(control_hwnd, SW_SHOW);
        
        //WTL::CString sHtmlPath;
        //sHtmlPath = IniTempHtmlFile();
        //dlg->Ini(sHtmlPath);
		//dlg->Create(_hSelf, NULL);
	};
    
protected :
	virtual BOOL CALLBACK run_dlgProc(UINT message, WPARAM wParam, LPARAM lParam);

private :

    /*
    int getLine() const {
        BOOL isSuccessful;
        int line = ::GetDlgItemInt(_hSelf, ID_GOLINE_EDIT, &isSuccessful, FALSE);
        return (isSuccessful?line:-1);
    };*/

};

#endif //NPPMARKDOWN2_DLG_H
