#ifndef CONTROL_DLG_H
#define CONTROL_DLG_H

#include "BaseDialog.h"

class ControlDlg : public CBaseDialog  
{
	public:

		ControlDlg(int nResId, HWND hParent=NULL, HINSTANCE hInst=NULL);
		virtual ~ControlDlg();

        void On_WM_INITDIALOG(HWND hDlg, WPARAM wParam, LPARAM lParam);
		void On_WM_COMMAND(HWND hDlg, WPARAM wParam, LPARAM lParam);
        
        virtual void Align(INT nWidth, INT nHeight);
        void OnBtnPreview();
		DECLARE_MESSAGE_HANDLER(ControlDlg);

};

#endif //CONTROL_DLG_H