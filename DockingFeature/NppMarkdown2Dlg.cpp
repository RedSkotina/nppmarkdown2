//this file is part of notepad++
//Copyright (C)2003 Don HO ( donho@altern.org )
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either
//version 2 of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#include "NppMarkdown2Dlg.h"
#include "PluginDefinition.h"

extern NppData nppData;

BOOL CALLBACK NppMarkdown2Dlg::run_dlgProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message) 
	{
		case WM_COMMAND : 
		{
			switch (wParam)
			{
				case IDOK :
				{
					/*int line = getLine();
					if (line != -1)
					{
						// Get the current scintilla
						int which = -1;
						::SendMessage(nppData._nppHandle, NPPM_GETCURRENTSCINTILLA, 0, (LPARAM)&which);
						if (which == -1)
							return FALSE;
						HWND curScintilla = (which == 0)?nppData._scintillaMainHandle:nppData._scintillaSecondHandle;

						::SendMessage(curScintilla, SCI_ENSUREVISIBLE, line-1, 0);
						::SendMessage(curScintilla, SCI_GOTOLINE, line-1, 0);
					}*/
					return TRUE;
				}
                
			}
				return FALSE;
		}
        case WM_SIZE:
        {
            if (control_dlg)
            {
                INT nWidth = LOWORD(lParam);
                INT nHeight = HIWORD(lParam);
                control_dlg->Align(nWidth, nHeight);
                //dlg->ShowWindow(SW_SHOW);
            }
            if (preview_dlg)
            {
                INT nWidth = LOWORD(lParam);
                INT nHeight = HIWORD(lParam);
                preview_dlg->Align(nWidth, nHeight);
                //dlg->ShowWindow(SW_SHOW);
            }
            return TRUE;
        }
        case NPPMARKDOWN2_PREVIEW:
        {
            preview_dlg->Preview();
            return TRUE;
        }
        case WM_DESTROY:
            delete preview_dlg;
            delete control_dlg;
            
            if (is_ole_owner)
                OleUninitialize();
            PostQuitMessage( 0 ); 
            return TRUE;
		default :
			return DockingDlgInterface::run_dlgProc(message, wParam, lParam);
	}
}

